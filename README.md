# Ubuntu 18.04 Jenkins build nodes

**Problem:**

In order to build the LCG software stack, the SPI team in EP-SFT needs a current Linux kernel which is not availble on CentOS 7 and installing a newer kernel from EPEL didn't work.  

**Approach:**

We want to use an Ubuntu LTS cloud image as long as CentOS 8 is not yet released. Ideally, we would use a Fedora LTS release since it is similar to CentOS, but Fedora doesn't provide long term support.  

**Scope:**

As Ubuntu is not officially supported by CERN IT, there are some functionalities that are hard to implement. For starters, we use local users - not the CERN users. We also don't have a shared HOME across multiple machines as we don't have AFS access. CVMFS read access is necessary and is provided by these scripts. Moreover we provide some base packages such as `cmake`, `git` etc.  
Most important is the installation of `docker-ce` as a host system for the LCG Docker images. Therefore we also need a working Kerberos environment (for `kinit` and `xrdcp`).  
Theoretically, there is also a fusex client for EOS but since it could not be setup properly yet it is not provided yet.  

The nodes are configured to install upgrades automatically and reboot if necessary at 22:50.

**Documentation:**

 - [CloudDocs: Create VM](http://clouddocs.web.cern.ch/clouddocs/tutorial/openstack_command_line.html)
 - [CloudDocs: VM Properties](http://clouddocs.web.cern.ch/clouddocs/using_openstack/properties.html)
 - [CloudDocs: VM Contextualisation](http://clouddocs.web.cern.ch/clouddocs/using_openstack/contextualisation.html)
 - [CloudDocs: Create Volume](http://clouddocs.web.cern.ch/clouddocs/tutorial/create_a_volume.html)
 - [CloudDocs: Volume Details](http://clouddocs.web.cern.ch/clouddocs/details/block_volumes.html)

## Prequisites

Download and save your personal OpenStack credentials from [CERN's OpenStack web interface](https://openstack.cern.ch/project/api_access/). Make sure you have selected the correct project from the dropdown menu on the top left before. Click on *DONWLOAD OPENSTACK RC FILE* and select *OPENSTACK RC FILE (IDENTITY API V3)*. Upload it to `lxplus-cloud` like 
```
scp ~/Downloads/PH\ LCGAA-openrc.sh lxplus-cloud.cern.ch:~/lcgapp-openstack-rc.sh
```

The **lxplus cloud** environment has the latest `openstack` software installed.
```
ssh lxplus-cloud.cern.ch 
```

On `lxplus-cloud` load the configuration with
```
source ~/lcgapp-openstack-rc.sh
```

(Optional) Create a project specific SSH key for OpenStack if necessary:
```
openstack keypair create lcgapp > ~/.ssh/lcgapp
chmod 600 ~/.ssh/lcgapp
```


## Add a new VM

**Image:** `lcgapp-ubuntu1804`  

List all available images:
```
openstack image list
```

**Flavor:** `m2.xlarge`  
 - 8CPU cores
 - Approximately 16 GB RAM

**Naming scheme:** `lcgapp-ubuntu1804-dockerhost-<two-digit number>`  

**Contextualisation:** `ubuntu1804-cloudinit.sh`
A script that is run by `cloud-init` right after the VM has been created as part of the initialization


### Create VM

Create a new VM on CERN's OpenStack infrastructure like this:
```
openstack server create \
    --key-name lcgapp \
    --flavor m2.xlarge \
    --image "lcgapp-ubuntu1804" \
    --user-data "ubuntu1804-cloudinit.sh" \
    --property description="Ubuntu 18.04 Docker host for Jenkins builds" \
    --property landb-description="EP-SFT Jenkins worker node for LCG" \
    --property landb-responsible="VOBOX-RESPONSIBLE-SFT" \
    --property landb-os="LINUX" \
    --property landb-osversion="18.04" \
    --property landb-ipv6ready=false \
    lcgapp-ubuntu1804-dockerhost-01
```

If the VM has been started successfully - which can take about 20 minutes - you can read the details:
```
openstack server show lcgapp-ubuntu1804-dockerhost-01
```


## Add a new volume

### Build disk

This disk is used to provide space for the Jenkins slave, workspace and temporary files.

 - **Type:** `io1` Since we have quota for high IO storage left and we expect high I/O during build time
 - **Size:** `320 GB` based on past configurations
 - **Naming scheme:** `lcgapp-ubuntu1804-dockerhost-<two-digit number>-build-volume`  

### Docker disk

This disk is used to store all Docker images and containers and avoid that the relatively small `/` partition doesn't get too crowded and thus makes the system unusable.

 - **Type:** `standard` Since we don't need a high I/O for Docker 
 - **Size:** `150 GB` based on past configurations
 - **Naming scheme:** `lcgapp-ubuntu1804-dockerhost-<two-digit number>-docker-volume`  

### Create a volume

In this example we create a build disk:

```
openstack volume create \
    --type io1 \
    --size 320 \
    --description "High IO disk for the Jenkins workspace on lcgapp-ubuntu1804-dockerhost-01" \
    lcgapp-ubuntu1804-dockerhost-01-build-volume
```

If the volume has been created successfully it should appear in the volume list:
```
openstack volume list
```

### Attach volume to VM

In this example we also use the build disk. Attach the volume to the VM that has been created in the previous step. Make sure that the numbers match (in this case `01`):
```
openstack server add volume lcgapp-ubuntu1804-dockerhost-01 lcgapp-ubuntu1804-dockerhost-01-build-volume
```

## All scripts in one

This is a summary of all of the above scripts to simplify the creation of new host machines. The following two bash functions are contained in `create_dockerhost.sh` for improved ease-of-use.


**Bash function to create a new compute instance and the corresponding volumes:**

```
function create_dockerhost {
    
    UBUNTU_DOCKERHOST="$1"
    
    echo "Creating instance ${UBUNTU_DOCKERHOST} ..."
    openstack server create \
        --key-name lcgapp \
        --flavor m2.xlarge \
        --image "lcgapp-ubuntu1804" \
        --user-data "ubuntu1804-cloudinit.sh" \
        --property description="Ubuntu 18.04 Docker host for Jenkins builds" \
        --property landb-description="EP-SFT Jenkins worker node for LCG" \
        --property landb-responsible="VOBOX-RESPONSIBLE-SFT" \
        --property landb-os="LINUX" \
        --property landb-osversion="18.04" \
        --property landb-ipv6ready=false \
        ${UBUNTU_DOCKERHOST}
    
    echo "Creating build volume ..."
    openstack volume create \
        --type io1 \
        --size 320 \
        --description "High IO disk for the Jenkins workspace on ${UBUNTU_DOCKERHOST}" \
        ${UBUNTU_DOCKERHOST}-build-volume
    
    echo "Creating docker volume ..."
    openstack volume create \
        --type standard \
        --size 150 \
        --description "Disk for the Docker images and containers on ${UBUNTU_DOCKERHOST}" \
        ${UBUNTU_DOCKERHOST}-docker-volume
}
```


**Bash function to attach the volumes to the new compute instance:**

```
function attach_volumes {
    
    UBUNTU_DOCKERHOST="$1"
    
    echo "Attaching build volume to instance ..."
    openstack server add volume \
        --device /dev/vdb \
        ${UBUNTU_DOCKERHOST} \
        ${UBUNTU_DOCKERHOST}-build-volume

    echo "Attaching docker volume to instance ..."
    openstack server add volume \
        --device /dev/vdc \
        ${UBUNTU_DOCKERHOST} \
        ${UBUNTU_DOCKERHOST}-docker-volume
}
```


**How to use the functions in the overall process:**

```
# 1) Optionally login to the project in OpenStack
source ~/lcgapp-openstack-rc.sh

# 2) Go into the main directory and load the two functions
cd ubuntu-cloud-init
source create_dockerhost.sh

# 3) Create new instances and volumes
create_dockerhost "lcgapp-ubuntu1804-dockerhost-03"
create_dockerhost "lcgapp-ubuntu1804-dockerhost-04"
create_dockerhost "lcgapp-ubuntu1804-dockerhost-05"
create_dockerhost "lcgapp-ubuntu1804-dockerhost-06"
create_dockerhost "lcgapp-ubuntu1804-dockerhost-07"

# 4) Wait some time and make sure that the instances are ready (status ACTIVE):
openstack server show "lcgapp-ubuntu1804-dockerhost-07" | grep status

# 5) Attach volumes to running instances
attach_volumes "lcgapp-ubuntu1804-dockerhost-03"
attach_volumes "lcgapp-ubuntu1804-dockerhost-04"
attach_volumes "lcgapp-ubuntu1804-dockerhost-05"
attach_volumes "lcgapp-ubuntu1804-dockerhost-06"
attach_volumes "lcgapp-ubuntu1804-dockerhost-07"

# 6) Upload ubuntu1804-firstboot.sh to each machine via scp,
# login via ssh and the lcgapp public key and execute the firstboot script
```
