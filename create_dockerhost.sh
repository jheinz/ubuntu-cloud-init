#!/bin/bash

set +x
set -e

function create_dockerhost {
    
    UBUNTU_DOCKERHOST="$1"
    
    echo "Creating instance ${UBUNTU_DOCKERHOST} ..."
    openstack server create \
        --key-name lcgapp \
        --flavor m2.xlarge \
        --image "lcgapp-ubuntu1804" \
        --user-data "ubuntu1804-cloudinit.sh" \
        --property description="Ubuntu 18.04 Docker host for Jenkins builds" \
        --property landb-description="EP-SFT Jenkins worker node for LCG" \
        --property landb-responsible="VOBOX-RESPONSIBLE-SFT" \
        --property landb-os="LINUX" \
        --property landb-osversion="18.04" \
        --property landb-ipv6ready=false \
        ${UBUNTU_DOCKERHOST}
    
    echo "Creating build volume ..."
    openstack volume create \
        --type io1 \
        --size 320 \
        --description "High IO disk for the Jenkins workspace on ${UBUNTU_DOCKERHOST}" \
        ${UBUNTU_DOCKERHOST}-build-volume
    
    echo "Creating docker volume ..."
    openstack volume create \
        --type standard \
        --size 150 \
        --description "Disk for the Docker images and containers on ${UBUNTU_DOCKERHOST}" \
        ${UBUNTU_DOCKERHOST}-docker-volume
}

function attach_volumes {
    
    UBUNTU_DOCKERHOST="$1"
    
    echo "Attaching build volume to instance ..."
    openstack server add volume \
        --device /dev/vdb \
        ${UBUNTU_DOCKERHOST} \
        ${UBUNTU_DOCKERHOST}-build-volume

    echo "Attaching docker volume to instance ..."
    openstack server add volume \
        --device /dev/vdc \
        ${UBUNTU_DOCKERHOST} \
        ${UBUNTU_DOCKERHOST}-docker-volume
}
